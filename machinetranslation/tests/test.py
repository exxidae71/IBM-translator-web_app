import unittest

from translator import english_to_french , french_to_english

class TestMyModule(unittest.TestCase):
    def test_eng(self):
        self.assertEqual(english_to_french('Hello') ,'Bonjour')
    def test_eng_null(self):
        self.assertEqual(english_to_french('321') ,'Bonjour')
    def test_fren(self):
        self.assertEqual(french_to_english('Bonjour') ,'Hello' )
    def test_fren_null(self):
        self.assertEqual(french_to_english('231') ,'Hello' )
if __name__ == '__main__':
    unittest.main()